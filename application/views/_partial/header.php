<header class="header fixed-top">
    <div class="container-fluid d-flex align-items-center justify-content-between bg-hero2 px-5">

      <a href="<?= base_url('index.php/administrator/dashboard') ?>" class="logo d-flex align-items-center">
        <img src="<?= base_url(); ?>assets/webaysite/assets/img/logo.png" alt=""><span class="text-white namesite">Webaysite</span>
      </a>

      <nav class="navbar">
        <ul>
          <li><a class="nav-link scrollto active text-white" href="index.html">Beranda</a></li>
          <li><a class="nav-link scrollto text-white" href="#">Artikel</a></li>
          <li><a class="nav-link scrollto text-white" href="#">Portofolio</a></li>
          <li><a class="nav-link scrollto text-white" href="#">Layanan</a></li>
          <li><a class="getstarted scrollto" href="<?= base_url('index.php/login') ?>">Get Started</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>

    </div>
  </header><!-- End Header -->
