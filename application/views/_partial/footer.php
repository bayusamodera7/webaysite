  <footer id="footer" class="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row gy-4">
          <div class="col-lg-5 col-md-12 footer-info">
            <a href="<?= base_url('index.php/administrator/dashboard') ?>" class="logo d-flex align-items-center">
              <img src="<?= base_url(); ?>assets/webaysite/assets/img/logo.png" alt="">
              <span>Webaysite</span>
            </a>
            <h5>Alamat</h5>
            <p>Perumahan Joyoagung Greenland No. B4-B5, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144.<br>
              08128518373 (Call)<br>
              08128518373 (WhatsApp)
            </p>
            <div class="social-links mt-3">
              <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
              <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
            </div>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Pusat Bantuan</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="#">FaQ</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Help</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Tentang Kami</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Hubungi Kami</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Paket Kami</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Paket Ekonomis</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Paket Bisnis</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Paket Premium</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Sitemap</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="#home">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#pricing">Harga Website</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#superiority">Keunggulan Fitur Website</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#testimonials">Testimoni Pelanggan</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#question">Tanya Jawab</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#footer">Footer</a></li>
            </ul>
          </div>

        </div>

      </div>
    </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Webaysite</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- anim JS Files -->
  <script src="<?= base_url(); ?>assets/webaysite/assets/anim/aos/aos.js"></script>
  <script src="<?= base_url(); ?>assets/webaysite/assets/anim/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url(); ?>assets/webaysite/assets/anim/glightbox/js/glightbox.min.js"></script>
  <script src="<?= base_url(); ?>assets/webaysite/assets/anim/swiper/swiper-bundle.min.js"></script>

  <!-- Main JS File -->
  <script src="<?= base_url(); ?>assets/webaysite/assets/js/main.js"></script>

</body>

</html>