<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Webaysite</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url(); ?>assets/webaysite/assets/img/logo.png" rel="shortcut icon" type="image/x-icon">

  <!-- anim CSS Files -->
  <link href="<?= base_url(); ?>assets/webaysite/assets/anim/aos/aos.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/webaysite/assets/anim/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/webaysite/assets/anim/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/webaysite/assets/anim/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url(); ?>assets/webaysite/assets/css/style.css" rel="stylesheet">


</head>